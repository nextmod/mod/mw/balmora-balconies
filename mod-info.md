# Name
Balmora Balconies

# Created by
Petethegoat

# Category
Miscellaneous

# Description
Adds a new open variety of the Hlaalu staircase piece.

# Release date
2019-06-04 17:43

# Last update date
2019-06-04 17:43

# Version
0.9

# Permissions
Everything here is public domain, and free, modify, or redistribute for any purpose, unless otherwise specified.
