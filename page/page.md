 

# Balmora Balconies

Requires Project Atlas.

Adds a new open variety of the Hlaalu staircase piece, and includes a plugin to replace the stairs used for the guilds in Balmora.
